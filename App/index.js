
/* Random */
var Random;

Random = function(min, max) {
  if (max == null) {
    max = min;
    min = 0;
  }
  return min + Math.random() * (max - min);
};

Random.int = function(min, max) {
  if (max == null) {
    max = min;
    min = 0;
  }
  return Math.floor(min + Math.random() * (max - min));
};

Random.sign = function(prob) {
  if (prob == null) {
    prob = 0.5;
  }
  if (Math.random() < prob) {
    return 1;
  } else {
    return -1;
  }
};

Random.bool = function(prob) {
  if (prob == null) {
    prob = 0.5;
  }
  return Math.random() < prob;
};

Random.item = function(list) {
  return list[Math.floor(Math.random() * list.length)];
};


/* 2D Vector */
var Vector;

Vector = (function() {

  /* Adds two vectors and returns the product. */
  Vector.add = function(v1, v2) {
    return new Vector(v1.x + v2.x, v1.y + v2.y);
  };


  /* Subtracts v2 from v1 and returns the product. */

  Vector.sub = function(v1, v2) {
    return new Vector(v1.x - v2.x, v1.y - v2.y);
  };


  /* Projects one vector (v1) onto another (v2) */

  Vector.project = function(v1, v2) {
    return v1.clone().scale((v1.dot(v2)) / v1.magSq());
  };


  /* Creates a new Vector instance. */

  function Vector(x, y) {
    this.x = x != null ? x : 0.0;
    this.y = y != null ? y : 0.0;
  }


  /* Sets the components of this vector. */

  Vector.prototype.set = function(x, y) {
    this.x = x;
    this.y = y;
    return this;
  };


  /* Add a vector to this one. */

  Vector.prototype.add = function(v) {
    this.x += v.x;
    this.y += v.y;
    return this;
  };


  /* Subtracts a vector from this one. */

  Vector.prototype.sub = function(v) {
    this.x -= v.x;
    this.y -= v.y;
    return this;
  };


  /* Scales this vector by a value. */

  Vector.prototype.scale = function(f) {
    this.x *= f;
    this.y *= f;
    return this;
  };


  /* Computes the dot product between vectors. */

  Vector.prototype.dot = function(v) {
    return this.x * v.x + this.y * v.y;
  };


  /* Computes the cross product between vectors. */

  Vector.prototype.cross = function(v) {
    return (this.x * v.y) - (this.y * v.x);
  };


  /* Computes the magnitude (length). */

  Vector.prototype.mag = function() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  };


  /* Computes the squared magnitude (length). */

  Vector.prototype.magSq = function() {
    return this.x * this.x + this.y * this.y;
  };


  /* Computes the distance to another vector. */

  Vector.prototype.dist = function(v) {
    var dx, dy;
    dx = v.x - this.x;
    dy = v.y - this.y;
    return Math.sqrt(dx * dx + dy * dy);
  };


  /* Computes the squared distance to another vector. */

  Vector.prototype.distSq = function(v) {
    var dx, dy;
    dx = v.x - this.x;
    dy = v.y - this.y;
    return dx * dx + dy * dy;
  };


  /* Normalises the vector, making it a unit vector (of length 1). */

  Vector.prototype.norm = function() {
    var m;
    m = Math.sqrt(this.x * this.x + this.y * this.y);
    this.x /= m;
    this.y /= m;
    return this;
  };


  /* Limits the vector length to a given amount. */

  Vector.prototype.limit = function(l) {
    var m, mSq;
    mSq = this.x * this.x + this.y * this.y;
    if (mSq > l * l) {
      m = Math.sqrt(mSq);
      this.x /= m;
      this.y /= m;
      this.x *= l;
      this.y *= l;
      return this;
    }
  };


  /* Copies components from another vector. */

  Vector.prototype.copy = function(v) {
    this.x = v.x;
    this.y = v.y;
    return this;
  };


  /* Clones this vector to a new itentical one. */

  Vector.prototype.clone = function() {
    return new Vector(this.x, this.y);
  };


  /* Resets the vector to zero. */

  Vector.prototype.clear = function() {
    this.x = 0.0;
    this.y = 0.0;
    return this;
  };

  Vector.prototype.toString = function() {
    return "" + this.x + ", " + this.y;
  };

  return Vector;

})();

var Color;

Color = (function() {
  function Color(r, g, b) {
    this.r = r != null ? r : 0;
    this.g = g != null ? g : 0;
    this.b = b != null ? b : 0;
    this.time = 0;
    this.duration = -1;
    this.callback = null;
  }

  Color.prototype.set = function(r, g, b) {
    this.r = r;
    this.g = g;
    this.b = b;
  };

  Color.prototype.isFading = function() {
    return this.time < this.duration;
  };

  Color.prototype.setFrom = function(c) {
    return this.set(c.r, c.g, c.b);
  };

  Color.prototype.clone = function() {
    return new Color(this.r, this.g, this.b);
  };

  Color.prototype.update = function(delta) {
    if (!this.isFading()) {
      return;
    }
    this.time += delta;
    this.r = Easie.linearNone(this.time, this.begin.r, this.change.r, this.duration);
    this.g = Easie.linearNone(this.time, this.begin.g, this.change.g, this.duration);
    this.b = Easie.linearNone(this.time, this.begin.b, this.change.b, this.duration);
    if (this.to.r > this.begin.r && this.r > this.to.r || this.to.r < this.begin.r && this.r < this.to.r) {
      this.r = this.to.r;
    }
    if (this.to.g > this.begin.g && this.g > this.to.g || this.to.g < this.begin.g && this.g < this.to.g) {
      this.g = this.to.g;
    }
    if (this.to.b > this.begin.b && this.b > this.to.b || this.to.b < this.begin.b && this.b < this.to.b) {
      this.b = this.to.b;
    }
    this.r = Math.round(this.r);
    this.g = Math.round(this.g);
    this.b = Math.round(this.b);
    if (this.time >= this.duration && this.callback) {
      return this.callback();
    }
  };

  Color.prototype.toString = function() {
    return "rgb(" + this.r + ", " + this.g + ", " + this.b + ")";
  };

  Color.prototype.fadeTo = function(to, duration, callback) {
    this.to = to;
    this.duration = duration != null ? duration : 1;
    this.callback = callback != null ? callback : null;
    this.time = 0;
    this.begin = this.clone();
    return this.change = new Color(this.to.r - this.begin.r, this.to.g - this.begin.g, this.to.b - this.begin.b);
  };

  return Color;

})();

this.Sound = (function() {
  function Sound() {}

  Sound.cache = {};

  Sound.PATH = 'assets/audio';

  Sound.load = function(id, file) {
    var audio, src;
    src = this.PATH + '/' + file;
    audio = new Audio(src);
    audio.preload = true;
    audio.load();
    return this.cache[id] = audio;
  };

  Sound.play = function(id, repeat) {
    var audio;
    if (!this.cache.hasOwnProperty(id)) {
      console.log("Unknown sound '" + id + "'");
      return;
    }
    audio = this.cache[id];
    if (repeat) {
      audio.loop = true;
    }
    return audio.play();
  };

  return Sound;

})();

var Entity;

Entity = (function() {
  function Entity(args) {
    if (args == null) {
      args = {};
    }
    this.pos = args.pos || new Vector(0, 0);
    this.rotation = args.rotation || 0;
    this.scale = args.scale || new Vector(1, 1);
    this.hidden = args.hidden || false;
    this.controllers = args.controllers || [];
  }

  Entity.prototype.clear = function() {
    return this.controllers = [];
  };

  Entity.prototype.attachController = function(controller) {
    return this.controllers.push(controller);
  };

  Entity.prototype.detachController = function(controller) {
    return this.controllers = this.controllers.filter(function(i) {
      return i !== controller;
    });
  };

  Entity.prototype.update = function(delta, inputs) {
    var controller, _i, _len, _ref, _results;
    _ref = this.controllers;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      controller = _ref[_i];
      _results.push(controller.update(delta, inputs));
    }
    return _results;
  };

  Entity.prototype.render = function(canvas, ctx) {
    ctx.translate(this.pos.x, this.pos.y);
    if (this.hFlipped) {
      ctx.rotate(this.rotation + Math.PI);
      return ctx.scale(-this.scale.x, this.scale.y);
    } else {
      ctx.rotate(this.rotation);
      return ctx.scale(this.scale.x, this.scale.y);
    }
  };

  Entity.prototype.animate = function(type, args) {
    var animation;
    if (args == null) {
      args = {};
    }
    if (type === 'wiggle') {
      args.type = 'rotate';
      args.f = args.f || Easie.elasticOut;
      args.begin = args.begin || -1;
      args.change = args.change || 1;
    } else if (type === 'rotate') {
      args.type = 'rotate';
      args.f = args.f || Easie.linearNone;
      args.begin = args.begin || this.rotation;
      args.change = args.change || Math.PI;
    } else if (type === 'shrink') {
      args.type = 'scale';
      args.f = args.f || Easie.linearNone;
      args.begin = args.begin || 1;
      args.change = args.change || -1;
    } else if (type === 'pulse') {
      args.type = 'scale';
      args.f = args.f || Easie.elasticOut;
      args.begin = args.begin || new Vector(0, 0);
      args.change = args.change || new Vector(1, 1);
    } else if (type === 'slide') {
      args.type = 'translate';
      args.f = args.f || Easie.linearNone;
      args.begin = args.begin || this.pos.clone();
      args.change = args.change || new Vector;
    }
    animation = new Animation(this, args);
    this.attachController(animation);
    return animation;
  };

  return Entity;

})();

var Node,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Node = (function(_super) {
  __extends(Node, _super);

  function Node(args) {
    if (args == null) {
      args = {};
    }
    Node.__super__.constructor.call(this, args);
    this.children = [];
  }

  Node.prototype.clear = function() {
    Node.__super__.clear.call(this);
    return this.children = [];
  };

  Node.prototype.attach = function(child) {
    if (!child) {
      console.log("Can not attach '" + child + "'");
      return;
    }
    return this.children.push(child);
  };

  Node.prototype.detach = function(child) {
    return this.children = this.children.filter(function(i) {
      return i !== child;
    });
  };

  Node.prototype.update = function(delta, inputs) {
    var child, _i, _len, _ref, _results;
    Node.__super__.update.call(this, delta, inputs);
    _ref = this.children;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      _results.push(child.update(delta, inputs));
    }
    return _results;
  };

  Node.prototype.render = function(canvas, ctx) {
    var child, _i, _len, _ref, _results;
    Node.__super__.render.call(this, canvas, ctx);
    _ref = this.children;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      if (child.hidden) {
        continue;
      }
      ctx.save();
      child.render(canvas, ctx);
      _results.push(ctx.restore());
    }
    return _results;
  };

  return Node;

})(Entity);

var Shape,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Shape = (function(_super) {
  __extends(Shape, _super);

  function Shape(args) {
    if (args == null) {
      args = {};
    }
    Shape.__super__.constructor.call(this, args);
    this.color = args.color || null;
    this.stroke = args.stroke || null;
    this.lineWidth = args.lineWidth || 1;
  }

  return Shape;

})(Entity);

var Circle,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Circle = (function(_super) {
  __extends(Circle, _super);

  function Circle(args) {
    if (args == null) {
      args = {};
    }
    Circle.__super__.constructor.call(this, args);
    this.radius = args.radius || 0;
  }

  Circle.prototype.render = function(canvas, ctx) {
    Circle.__super__.render.call(this, canvas, ctx);
    ctx.beginPath();
    ctx.arc(0, 0, this.radius, 0, Math.PI * 2, false);
    ctx.closePath();
    if (this.color) {
      ctx.fillStyle = this.color.toString();
      ctx.fill();
    }
    if (this.stroke) {
      ctx.strokeStyle = this.stroke.toString();
      return ctx.stroke();
    }
  };

  Circle.prototype.intersectsDot = function(x, y) {
    var d, xDist, yDist;
    xDist = Math.abs(x - this.pos.x);
    yDist = Math.abs(y - this.pos.y);
    d = Math.sqrt((xDist * xDist) + (yDist * yDist));
    return d < this.radius;
  };

  Circle.prototype.intersectsCircle = function(other) {
    var d, xDist, yDist;
    xDist = Math.abs(other.x - this.pos.x);
    yDist = Math.abs(other.y - this.pos.y);
    d = Math.sqrt((xDist * xDist) + (yDist * yDist));
    return d < (this.radius + other.radius);
  };

  return Circle;

})(Shape);

var ProgressArc,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

ProgressArc = (function(_super) {
  __extends(ProgressArc, _super);

  function ProgressArc(args) {
    if (args == null) {
      args = {};
    }
    ProgressArc.__super__.constructor.call(this, args);
    this.radius = args.radius || 10;
    this.percent = args.percent || 1;
  }

  ProgressArc.prototype.render = function(canvas, ctx) {
    var endAngle, startAngle;
    ProgressArc.__super__.render.call(this, canvas, ctx);
    startAngle = 1.5 * Math.PI;
    endAngle = startAngle - (this.percent * Math.PI * 2);
    ctx.beginPath();
    ctx.arc(0, -this.lineWidth / 2, this.radius, startAngle, endAngle, false);
    ctx.lineWidth = this.lineWidth;
    ctx.strokeStyle = this.color.toString();
    return ctx.stroke();
  };

  return ProgressArc;

})(Shape);

var Rect,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Rect = (function(_super) {
  __extends(Rect, _super);

  function Rect(args) {
    if (args == null) {
      args = {};
    }
    Rect.__super__.constructor.call(this, args);
    this.w = args.w;
    this.h = args.h;
  }

  Rect.prototype.intersectsDot = function(x, y) {
    var bottom, left, right, top;
    left = this.pos.x - this.w / 2;
    top = this.pos.y - this.h / 2;
    bottom = top + this.h;
    right = left + this.w;
    return x > left && x < right && y > top && y < bottom;
  };

  Rect.prototype.render = function(canvas, ctx) {
    Rect.__super__.render.call(this, canvas, ctx);
    if (this.color) {
      ctx.fillStyle = this.color.toString();
      ctx.fillRect(this.w / -2, this.h / -2, this.w, this.h);
    }
    if (this.stroke) {
      ctx.strokeStyle = this.stroke.toString();
      ctx.lineWidth = this.lineWidth;
      return ctx.strokeRect(this.w / -2, this.h / -2, this.w, this.h);
    }
  };

  return Rect;

})(Shape);

var RoundedRect,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

RoundedRect = (function(_super) {
  __extends(RoundedRect, _super);

  function RoundedRect(args) {
    if (args == null) {
      args = {};
    }
    RoundedRect.__super__.constructor.call(this, args);
    this.w = args.w;
    this.h = args.h;
    this.r = args.r;
  }

  RoundedRect.prototype.render = function(canvas, ctx) {
    var h, r, w, x, y;
    RoundedRect.__super__.render.call(this, canvas, ctx);
    w = this.w;
    h = this.h;
    x = w / -2;
    y = h / -2;
    r = this.r;
    ctx.beginPath();
    ctx.moveTo(x + r, y);
    ctx.arcTo(x + w, y, x + w, y + h, r);
    ctx.arcTo(x + w, y + h, x, y + h, r);
    ctx.arcTo(x, y + h, x, y, r);
    ctx.arcTo(x, y, x + w, y, r);
    ctx.closePath();
    if (this.color) {
      ctx.fillStyle = this.color.toString();
      ctx.fill();
    }
    if (this.stroke) {
      ctx.strokeStyle = this.stroke.toString();
      ctx.lineWidth = this.lineWidth;
      return ctx.stroke();
    }
  };

  return RoundedRect;

})(Shape);

var Polygon,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Polygon = (function(_super) {
  __extends(Polygon, _super);

  function Polygon(args) {
    if (args == null) {
      args = {};
    }
    Polygon.__super__.constructor.call(this, args);
    this.vertices = args.vertices || [];
  }

  Polygon.prototype.render = function(canvas, ctx) {
    var vertex, _i, _len, _ref;
    Polygon.__super__.render.call(this, canvas, ctx);
    ctx.fillStyle = this.color.toString();
    ctx.beginPath();
    _ref = this.vertices;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      vertex = _ref[_i];
      ctx.lineTo(vertex.x, vertex.y);
    }
    ctx.closePath();
    return ctx.fill();
  };

  return Polygon;

})(Shape);

var Line,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Line = (function(_super) {
  __extends(Line, _super);

  function Line(args) {
    if (args == null) {
      args = {};
    }
    Line.__super__.constructor.call(this, args);
    this.to = args.to;
  }

  Line.prototype.render = function(canvas, ctx) {
    Line.__super__.render.call(this, canvas, ctx);
    ctx.strokeStyle = this.color.toString();
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(this.to.x, this.to.y);
    return ctx.stroke();
  };

  return Line;

})(Shape);

var Text,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Text = (function(_super) {
  __extends(Text, _super);

  function Text(args) {
    if (args == null) {
      args = {};
    }
    args.color = args.color || CFG.STD_FG_COLOR.clone();
    Text.__super__.constructor.call(this, args);
    this.text = args.text || '';
    this.textAlign = args.textAlign || 'center';
    this.textBaseline = args.textBaseline || 'middle';
    this.fontSize = args.fontSize || CFG.STD_FONT_SIZE;
    this.font = args.font || CFG.STD_FONT;
  }

  Text.prototype.render = function(canvas, ctx) {
    Text.__super__.render.call(this, canvas, ctx);
    ctx.font = this.fontSize + 'px ' + this.font;
    ctx.textAlign = this.textAlign;
    ctx.textBaseline = this.textBaseline;
    ctx.lineWidth = this.lineWidth;
    if (this.color) {
      ctx.fillStyle = this.color.toString();
      ctx.fillText(this.text, 0, 0);
    }
    if (this.stroke) {
      ctx.strokeStyle = this.stroke.toString();
      return ctx.strokeText(this.text, 0, 0);
    }
  };

  return Text;

})(Shape);

var Img,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Img = (function(_super) {
  __extends(Img, _super);

  Img.cache = {};

  function Img(args) {
    var cache, src;
    if (args == null) {
      args = {};
    }
    Img.__super__.constructor.call(this, args);
    src = args.src;
    this.w = args.w;
    this.h = args.h;
    cache = this.constructor.cache;
    if (cache.hasOwnProperty(src)) {
      this.img = cache[src];
    } else {
      this.img = new Image();
      this.img.src = src;
      cache[src] = this.img;
    }
  }

  Img.prototype.render = function(canvas, ctx) {
    var h, w;
    Img.__super__.render.call(this, canvas, ctx);
    if (!this.img.width && this.img.height) {
      return;
    }
    w = this.w || this.img.width;
    h = this.w || this.img.height;
    return ctx.drawImage(this.img, w / -2, h / -2, w, h);
  };

  return Img;

})(Entity);

var Button,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Button = (function(_super) {
  __extends(Button, _super);

  function Button(args) {
    Button.__super__.constructor.call(this);
    this.img1 = new Img({
      src: args.src1,
      hidden: false
    });
    this.img2 = new Img({
      src: args.src2,
      hidden: true
    });
    this.attach(this.img1);
    this.attach(this.img2);
  }

  Button.prototype.press = function() {
    this.img1.hidden = true;
    return this.img2.hidden = false;
  };

  return Button;

})(Node);


/*
Easie.coffee (https://github.com/jimjeffers/Easie)
Project created by J. Jeffers

Robert Penner's Easing Equations in CoffeeScript
http://robertpenner.com/easing/

DISCLAIMER: Software provided as is with no warranty of any type. 
Don't do bad things with this :)
 */
this.Easie = (function() {
  function Easie() {}

  Easie.backIn = function(time, begin, change, duration, overshoot) {
    if (overshoot == null) {
      overshoot = 1.70158;
    }
    return change * (time /= duration) * time * ((overshoot + 1) * time - overshoot) + begin;
  };

  Easie.backOut = function(time, begin, change, duration, overshoot) {
    if (overshoot == null) {
      overshoot = 1.70158;
    }
    return change * ((time = time / duration - 1) * time * ((overshoot + 1) * time + overshoot) + 1) + begin;
  };

  Easie.backInOut = function(time, begin, change, duration, overshoot) {
    if (overshoot == null) {
      overshoot = 1.70158;
    }
    if ((time = time / (duration / 2)) < 1) {
      return change / 2 * (time * time * (((overshoot *= 1.525) + 1) * time - overshoot)) + begin;
    } else {
      return change / 2 * ((time -= 2) * time * (((overshoot *= 1.525) + 1) * time + overshoot) + 2) + begin;
    }
  };

  Easie.bounceOut = function(time, begin, change, duration) {
    if ((time /= duration) < 1 / 2.75) {
      return change * (7.5625 * time * time) + begin;
    } else if (time < 2 / 2.75) {
      return change * (7.5625 * (time -= 1.5 / 2.75) * time + 0.75) + begin;
    } else if (time < 2.5 / 2.75) {
      return change * (7.5625 * (time -= 2.25 / 2.75) * time + 0.9375) + begin;
    } else {
      return change * (7.5625 * (time -= 2.625 / 2.75) * time + 0.984375) + begin;
    }
  };

  Easie.bounceIn = function(time, begin, change, duration) {
    return change - Easie.bounceOut(duration - time, 0, change, duration) + begin;
  };

  Easie.bounceInOut = function(time, begin, change, duration) {
    if (time < duration / 2) {
      return Easie.bounceIn(time * 2, 0, change, duration) * 0.5 + begin;
    } else {
      return Easie.bounceOut(time * 2 - duration, 0, change, duration) * 0.5 + change * 0.5 + begin;
    }
  };

  Easie.circIn = function(time, begin, change, duration) {
    return -change * (Math.sqrt(1 - (time = time / duration) * time) - 1) + begin;
  };

  Easie.circOut = function(time, begin, change, duration) {
    return change * Math.sqrt(1 - (time = time / duration - 1) * time) + begin;
  };

  Easie.circInOut = function(time, begin, change, duration) {
    if ((time = time / (duration / 2)) < 1) {
      return -change / 2 * (Math.sqrt(1 - time * time) - 1) + begin;
    } else {
      return change / 2 * (Math.sqrt(1 - (time -= 2) * time) + 1) + begin;
    }
  };

  Easie.cubicIn = function(time, begin, change, duration) {
    return change * (time /= duration) * time * time + begin;
  };

  Easie.cubicOut = function(time, begin, change, duration) {
    return change * ((time = time / duration - 1) * time * time + 1) + begin;
  };

  Easie.cubicInOut = function(time, begin, change, duration) {
    if ((time = time / (duration / 2)) < 1) {
      return change / 2 * time * time * time + begin;
    } else {
      return change / 2 * ((time -= 2) * time * time + 2) + begin;
    }
  };

  Easie.elasticOut = function(time, begin, change, duration, amplitude, period) {
    var overshoot;
    if (amplitude == null) {
      amplitude = null;
    }
    if (period == null) {
      period = null;
    }
    if (time === 0) {
      return begin;
    } else if ((time = time / duration) === 1) {
      return begin + change;
    } else {
      if (period == null) {
        period = duration * 0.3;
      }
      if ((amplitude == null) || amplitude < Math.abs(change)) {
        amplitude = change;
        overshoot = period / 4;
      } else {
        overshoot = period / (2 * Math.PI) * Math.asin(change / amplitude);
      }
      return (amplitude * Math.pow(2, -10 * time)) * Math.sin((time * duration - overshoot) * (2 * Math.PI) / period) + change + begin;
    }
  };

  Easie.elasticIn = function(time, begin, change, duration, amplitude, period) {
    var overshoot;
    if (amplitude == null) {
      amplitude = null;
    }
    if (period == null) {
      period = null;
    }
    if (time === 0) {
      return begin;
    } else if ((time = time / duration) === 1) {
      return begin + change;
    } else {
      if (period == null) {
        period = duration * 0.3;
      }
      if ((amplitude == null) || amplitude < Math.abs(change)) {
        amplitude = change;
        overshoot = period / 4;
      } else {
        overshoot = period / (2 * Math.PI) * Math.asin(change / amplitude);
      }
      time -= 1;
      return -(amplitude * Math.pow(2, 10 * time)) * Math.sin((time * duration - overshoot) * (2 * Math.PI) / period) + begin;
    }
  };

  Easie.elasticInOut = function(time, begin, change, duration, amplitude, period) {
    var overshoot;
    if (amplitude == null) {
      amplitude = null;
    }
    if (period == null) {
      period = null;
    }
    if (time === 0) {
      return begin;
    } else if ((time = time / (duration / 2)) === 2) {
      return begin + change;
    } else {
      if (period == null) {
        period = duration * (0.3 * 1.5);
      }
      if ((amplitude == null) || amplitude < Math.abs(change)) {
        amplitude = change;
        overshoot = period / 4;
      } else {
        overshoot = period / (2 * Math.PI) * Math.asin(change / amplitude);
      }
      if (time < 1) {
        return -0.5 * (amplitude * Math.pow(2, 10 * (time -= 1))) * Math.sin((time * duration - overshoot) * ((2 * Math.PI) / period)) + begin;
      } else {
        return amplitude * Math.pow(2, -10 * (time -= 1)) * Math.sin((time * duration - overshoot) * (2 * Math.PI) / period) + change + begin;
      }
    }
  };

  Easie.expoIn = function(time, begin, change, duration) {
    if (time === 0) {
      return begin;
    }
    return change * Math.pow(2, 10 * (time / duration - 1)) + begin;
  };

  Easie.expoOut = function(time, begin, change, duration) {
    if (time === duration) {
      return begin + change;
    }
    return change * (-Math.pow(2, -10 * time / duration) + 1) + begin;
  };

  Easie.expoInOut = function(time, begin, change, duration) {
    if (time === 0) {
      return begin;
    } else if (time === duration) {
      return begin + change;
    } else if ((time = time / (duration / 2)) < 1) {
      return change / 2 * Math.pow(2, 10 * (time - 1)) + begin;
    } else {
      return change / 2 * (-Math.pow(2, -10 * (time - 1)) + 2) + begin;
    }
  };

  Easie.linearNone = function(time, begin, change, duration) {
    return change * time / duration + begin;
  };

  Easie.linearIn = function(time, begin, change, duration) {
    return Easie.linearNone(time, begin, change, duration);
  };

  Easie.linearOut = function(time, begin, change, duration) {
    return Easie.linearNone(time, begin, change, duration);
  };

  Easie.linearInOut = function(time, begin, change, duration) {
    return Easie.linearNone(time, begin, change, duration);
  };

  Easie.quadIn = function(time, begin, change, duration) {
    return change * (time = time / duration) * time + begin;
  };

  Easie.quadOut = function(time, begin, change, duration) {
    return -change * (time = time / duration) * (time - 2) + begin;
  };

  Easie.quadInOut = function(time, begin, change, duration) {
    if ((time = time / (duration / 2)) < 1) {
      return change / 2 * time * time + begin;
    } else {
      return -change / 2 * ((time -= 1) * (time - 2) - 1) + begin;
    }
  };

  Easie.quartIn = function(time, begin, change, duration) {
    return change * (time = time / duration) * time * time * time + begin;
  };

  Easie.quartOut = function(time, begin, change, duration) {
    return -change * ((time = time / duration - 1) * time * time * time - 1) + begin;
  };

  Easie.quartInOut = function(time, begin, change, duration) {
    if ((time = time / (duration / 2)) < 1) {
      return change / 2 * time * time * time * time + begin;
    } else {
      return -change / 2 * ((time -= 2) * time * time * time - 2) + begin;
    }
  };

  Easie.quintIn = function(time, begin, change, duration) {
    return change * (time = time / duration) * time * time * time * time + begin;
  };

  Easie.quintOut = function(time, begin, change, duration) {
    return change * ((time = time / duration - 1) * time * time * time * time + 1) + begin;
  };

  Easie.quintInOut = function(time, begin, change, duration) {
    if ((time = time / (duration / 2)) < 1) {
      return change / 2 * time * time * time * time * time + begin;
    } else {
      return change / 2 * ((time -= 2) * time * time * time * time + 2) + begin;
    }
  };

  Easie.sineIn = function(time, begin, change, duration) {
    return -change * Math.cos(time / duration * (Math.PI / 2)) + change + begin;
  };

  Easie.sineOut = function(time, begin, change, duration) {
    return change * Math.sin(time / duration * (Math.PI / 2)) + begin;
  };

  Easie.sineInOut = function(time, begin, change, duration) {
    return -change / 2 * (Math.cos(Math.PI * time / duration) - 1) + begin;
  };

  return Easie;

})();

var Animation;

Animation = (function() {
  function Animation(target, args) {
    this.target = target;
    if (args == null) {
      args = {};
    }
    this.type = args.type || null;
    this.f = args.f || Easie.linearNone;
    this.begin = args.begin || null;
    this.change = args.change || null;
    this.duration = args.duration || 1;
    this.repeat = args.repeat || false;
    this.skipFirstRun = args.skipFirstRun || false;
    this.onDone = args.onDone || null;
    this.onWakeUp = args.onWakeUp || null;
    this.reset();
    if (this.skipFirstRun) {
      this.dead = true;
      this.repeatIn = this.repeat;
    }
  }

  Animation.prototype.reset = function() {
    this.current = 0;
    this.repeatIn = 0;
    this.paused = false;
    return this.dead = false;
  };

  Animation.prototype.wakeUp = function() {
    this.reset();
    if (this.onWakeUp) {
      return this.onWakeUp(this);
    }
  };

  Animation.prototype.pause = function() {
    return this.paused = true;
  };

  Animation.prototype.resume = function() {
    return this.paused = false;
  };

  Animation.prototype.update = function(delta) {
    if (this.paused) {
      return;
    }
    if (this.dead) {
      if (this.repeatIn > 0) {
        this.repeatIn -= delta;
        if (this.repeatIn <= 0) {
          this.wakeUp();
        }
      }
      return;
    }
    this.current += delta;
    if (this.current > this.duration) {
      this.dead = true;
      if (this.onDone) {
        this.onDone(this);
      }
      if (this.repeat) {
        this.repeatIn = this.repeat;
      }
      return;
    }
    return this.step();
  };

  Animation.prototype.step = function() {
    if (this.type === 'translate') {
      this.target.pos.x = this.f(this.current, this.begin.x, this.change.x, this.duration);
      return this.target.pos.y = this.f(this.current, this.begin.y, this.change.y, this.duration);
    } else if (this.type === 'rotate') {
      return this.target.rotation = this.f(this.current, this.begin, this.change, this.duration);
    } else if (this.type === 'scale') {
      this.target.scale.x = this.f(this.current, this.begin.x, this.change.x, this.duration);
      return this.target.scale.y = this.f(this.current, this.begin.y, this.change.y, this.duration);
    } else {
      return console.log("Unknown animation type '" + this.type + "'");
    }
  };

  return Animation;

})();

var InputHandler;

InputHandler = (function() {
  function InputHandler() {
    document.addEventListener('touchstart', (function(_this) {
      return function(ev) {
        var args;
        args = {
          x: ev.touches[0].pageX - APP.w / 2,
          y: ev.touches[0].pageY - APP.h / 2
        };
        return _this.exec('tap', args);
      };
    })(this));
    document.addEventListener('mousedown', (function(_this) {
      return function(ev) {
        var args;
        args = {
          x: ev.x - APP.w / 2,
          y: ev.y - APP.h / 2
        };
        return _this.exec('tap', args);
      };
    })(this));
    this.inputs = [];
  }

  InputHandler.prototype.getRecordedInputs = function() {
    return this.inputs;
  };

  InputHandler.prototype.reset = function() {
    return this.inputs = [];
  };

  InputHandler.prototype.exec = function(command, args) {
    return this.inputs.push({
      command: command,
      args: args
    });
  };

  return InputHandler;

})();

var DEBUG, DebugDisplay,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

DEBUG = function(text) {
  return APP.debug.setText(text);
};

DebugDisplay = (function(_super) {
  __extends(DebugDisplay, _super);

  function DebugDisplay() {
    var FONT_SIZE;
    DebugDisplay.__super__.constructor.call(this, {
      pos: new Vector(-APP.w / 2, APP.h / 2)
    });
    this.STAY_TIME = 2;
    FONT_SIZE = 20;
    this.text = new Text({
      text: 'DebugDisplay',
      textAlign: 'left',
      pos: new Vector(0, -FONT_SIZE / 2),
      color: new Color(0, 0, 0),
      font: 'Monospace',
      fontSize: FONT_SIZE
    });
    this.attach(this.text);
    this.time = this.STAY_TIME;
    this.hidden = true;
  }

  DebugDisplay.prototype.setText = function(text) {
    this.time = 0;
    this.hidden = false;
    return this.text.text = text;
  };

  DebugDisplay.prototype.update = function(delta, inputs) {
    if (!CFG.DEBUG_ENABLED) {
      return;
    }
    if (this.hidden) {
      return;
    }
    DebugDisplay.__super__.update.call(this, delta, inputs);
    this.time += delta;
    return this.hidden = this.time > this.STAY_TIME;
  };

  DebugDisplay.prototype.render = function(canvas, ctx) {
    if (!CFG.DEBUG_ENABLED) {
      return;
    }
    if (this.hidden) {
      return;
    }
    return DebugDisplay.__super__.render.call(this, canvas, ctx);
  };

  return DebugDisplay;

})(Node);

var Background;

Background = (function() {
  function Background() {
    this.color = CFG.STD_BG_COLOR.clone();
  }

  Background.prototype.update = function(delta) {
    return this.color.update(delta);
  };

  Background.prototype.render = function(canvas, ctx) {
    var left, top;
    left = -APP.w / 2;
    top = -APP.h / 2;
    ctx.fillStyle = this.color.toString();
    return ctx.fillRect(left, top, APP.w, APP.h);
  };

  return Background;

})();

var Application;

Application = (function() {
  function Application(canvas) {
    var file, id, _ref;
    this.canvas = canvas;
    console.log('application started');
    this.ctx = this.canvas.getContext('2d');
    this.w = window.innerWidth;
    this.h = window.innerHeight;
    if (typeof ejecta === "undefined") {
      this.retinaCanvasHack();
      window.onresize = (function(_this) {
        return function() {
          return _this.retinaCanvasHack();
        };
      })(this);
    }
    this.ctx = this.canvas.getContext('2d');
    _ref = CFG.SOUNDS;
    for (id in _ref) {
      file = _ref[id];
      Sound.load(id, file);
    }
  }

  Application.prototype.start = function() {
    this.input = new InputHandler;
    this.background = new Background;
    this.debug = new DebugDisplay;
    this.root = new RootNode;
    return this.root.start();
  };

  Application.prototype.retinaCanvasHack = function() {
    this.scaleFactor = window.devicePixelRatio ? window.devicePixelRatio : 1;
    console.log('scaleFactor', this.scaleFactor);
    this.canvas.width = this.w * this.scaleFactor;
    this.canvas.height = this.h * this.scaleFactor;
    this.canvas.style.width = this.w + 'px';
    this.canvas.style.height = this.h + 'px';
    return this.ctx.scale(this.scaleFactor, this.scaleFactor);
  };

  Application.prototype.update = function() {
    var delta, inputs, now;
    now = new Date().getTime();
    delta = this.last ? (now - this.last) / 1000 : 0;
    inputs = this.input.getRecordedInputs();
    this.input.reset();
    this.background.update(delta, inputs);
    this.root.update(delta, inputs);
    this.debug.update(delta, inputs);
    return this.last = now;
  };

  Application.prototype.render = function() {
    this.ctx.save();
    this.ctx.translate(this.w / 2, this.h / 2);
    this.background.render(this.canvas, this.ctx);
    this.root.render(this.canvas, this.ctx);
    this.debug.render(this.canvas, this.ctx);
    return this.ctx.restore();
  };

  return Application;

})();

var CFG;

CFG = {
  DEBUG_ENABLED: true,
  STD_BG_COLOR: new Color(120, 120, 120),
  STD_FG_COLOR: new Color(0, 0, 0),
  STD_LINE_WIDTH: 4,
  STD_FONT: 'Lemon-Regular',
  STD_FONT_SIZE: 20,
  STD_FONT_SIZE_BIG: 50,
  GAME_TIME: 10,
  TAP_POWER: 4,
  SKIN_COLORS: [new Color(251, 71, 87), new Color(136, 225, 80)],
  PADDING: 8,
  SONG_TITLES: {
    'song1': 'Professor Kliq - Bust This Bust That'
  },
  SOUNDS: {
    'song1': 'laut.mp3'
  }
};

if (window.innerWidth >= 768) {
  CFG.STD_FONT_SIZE = 30;
  CFG.STD_FONT_SIZE_BIG = 70;
}

if (typeof ejecta !== "undefined") {
  ejecta.loadFont("assets/fonts/" + CFG.STD_FONT + ".ttf");
}

var RootNode,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

RootNode = (function(_super) {
  __extends(RootNode, _super);

  function RootNode() {
    RootNode.__super__.constructor.call(this);
    this.scene1 = new Scene1({
      pos: new Vector
    });
    this.scene2 = new Scene2({
      pos: new Vector(0, APP.h)
    });
    this.attach(this.scene1);
    this.attach(this.scene2);
  }

  RootNode.prototype.start = function() {
    return this.animate('slide', {
      duration: 3,
      onDone: (function(_this) {
        return function() {
          return _this.animate('slide', {
            change: new Vector(0, -APP.h),
            f: Easie.cubicOut,
            duration: 5,
            onDone: function() {
              return _this.scene2.start();
            }
          });
        };
      })(this)
    });
  };

  return RootNode;

})(Node);

var Scene1,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Scene1 = (function(_super) {
  __extends(Scene1, _super);

  function Scene1(args) {
    var skyColor;
    Scene1.__super__.constructor.call(this, args);
    skyColor = new Color(49, 163, 233);
    this.attach(new Rect({
      color: skyColor.clone(),
      w: APP.w,
      h: APP.h
    }));
    this.attach(new Text({
      text: "On the surface",
      pos: new Vector(0, -50)
    }));
    this.t2 = new Text({
      text: "Everything looks calm",
      pos: new Vector(0, 0),
      color: skyColor.clone()
    });
    this.animate('slide', {
      duration: 1.5,
      onDone: (function(_this) {
        return function() {
          return _this.t2.color.fadeTo(CFG.STD_FG_COLOR);
        };
      })(this)
    });
    this.attach(this.t2);
  }

  Scene1.prototype.update = function(delta) {
    Scene1.__super__.update.call(this, delta);
    return this.t2.color.update(delta);
  };

  return Scene1;

})(Node);

var CircButton, Indicator, Scene2,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Indicator = (function(_super) {
  __extends(Indicator, _super);

  function Indicator(args) {
    var H, W;
    Indicator.__super__.constructor.call(this, args);
    W = 15;
    H = 15;
    this.player1 = args.player1;
    this.player2 = args.player2;
    this.attach(new Polygon({
      color: CFG.STD_FG_COLOR,
      vertices: [new Vector(W / -2, H / -2), new Vector(W / 2, H / -2), new Vector(0, H / 2)]
    }));
    this.attach(new Text({
      text: 'Winner',
      pos: new Vector(0, -25),
      fontSize: 16
    }));
    this.paused = false;
  }

  Indicator.prototype.getWinner = function() {
    if (this.pos.x > 0) {
      return this.player2;
    }
    return this.player1;
  };

  Indicator.prototype.update = function(delta) {
    var d, newX;
    Indicator.__super__.update.call(this, delta);
    if (this.paused) {
      return;
    }
    newX = this.player2.getScore() - this.player1.getScore();
    if (newX < this.player1.pos.x) {
      newX = this.player1.pos.x;
    }
    if (newX > this.player2.pos.x) {
      newX = this.player2.pos.x;
    }
    d = newX - this.pos.x;
    return this.pos.x += d * delta;
  };

  return Indicator;

})(Node);

CircButton = (function(_super) {
  __extends(CircButton, _super);

  function CircButton(args) {
    CircButton.__super__.constructor.call(this, args);
    this.callback = args.callback;
    this.n = new Node;
    this.c1 = new Circle({
      color: new Color(200, 200, 200),
      radius: 40
    });
    this.c2 = new Circle({
      color: new Color(20, 20, 20),
      radius: 40,
      pos: new Vector(0, 5)
    });
    this.attach(this.c2);
    this.attach(this.n);
    this.n.attach(this.c1);
    this.n.attach(args.icon);
  }

  CircButton.prototype.down = function() {
    this.controllers = [];
    this.n.pos.y = 0;
    return this.n.animate('slide', {
      duration: 0.1,
      change: new Vector(0, 5),
      onDone: (function(_this) {
        return function() {
          return _this.up();
        };
      })(this)
    });
  };

  CircButton.prototype.up = function() {
    this.controllers = [];
    this.n.pos.y = 5;
    return this.n.animate('slide', {
      duration: 0.1,
      change: new Vector(0, -5)
    });
  };

  CircButton.prototype.tap = function(x, y) {
    var clicked;
    x = this.pos.x - x;
    y = this.pos.y - y;
    clicked = this.c1.intersectsDot(x, y);
    if (clicked) {
      this.down();
      this.callback();
    }
    return clicked;
  };

  return CircButton;

})(Node);

Scene2 = (function(_super) {
  __extends(Scene2, _super);

  function Scene2(args) {
    Scene2.__super__.constructor.call(this, args);
    this.controls = new Node({
      pos: new Vector(0, APP.h / 2 + 150)
    });
    this.playAgain = new Text({
      text: 'Play again',
      pos: new Vector(0, APP.h / 2 - 50),
      hidden: true
    });
    this.btn1 = new CircButton({
      pos: new Vector(-50, 0),
      icon: new Head({
        skinColor: CFG.SKIN_COLORS[0],
        scale: new Vector(0.5, 0.5)
      }),
      callback: (function(_this) {
        return function() {
          return _this.player1.wobbleHead();
        };
      })(this)
    });
    this.btn2 = new CircButton({
      pos: new Vector(50, 0),
      icon: new Player({
        skinColor: CFG.SKIN_COLORS[0],
        scale: new Vector(0.3, 0.3)
      }),
      callback: (function(_this) {
        return function() {
          return _this.player1.wobbleBody();
        };
      })(this)
    });
    this.player1 = new Player({
      skinColor: CFG.SKIN_COLORS[0],
      pos: new Vector(-100, 0)
    });
    this.player2 = new AIPlayer({
      skinColor: CFG.SKIN_COLORS[1],
      handicap: 0.1,
      pos: new Vector(100, 0)
    });
    this.indicator = new Indicator({
      player1: this.player1,
      player2: this.player2,
      pos: new Vector(0, -150),
      hidden: true
    });
    this.attach(this.player1);
    this.attach(this.player2);
    this.controls.attach(this.btn1);
    this.controls.attach(this.btn2);
    this.attach(this.indicator);
    this.attach(this.controls);
    this.attach(this.playAgain);
    this.introText = new Text({
      text: "But under, there's a battle",
      pos: new Vector(0, -APP.h / 2 + CFG.STD_FONT_SIZE + CFG.PADDING)
    });
    this.attach(this.introText);
  }

  Scene2.prototype.start = function() {
    this.introText.color.fadeTo(CFG.STD_BG_COLOR);
    return this.animate('slide', {
      duration: 1.5,
      onDone: (function(_this) {
        return function() {
          return _this.reset();
        };
      })(this)
    });
  };

  Scene2.prototype.reset = function() {
    Sound.play('song1', true);
    this.playAgain.hidden = true;
    this.player1.reset();
    this.player2.reset();
    this.player1.pos = new Vector(-100, 0);
    this.player2.pos = new Vector(100, 0);
    this.introText.hidden = true;
    this.player2.start();
    this.time = 0;
    this.indicator.hidden = false;
    this.indicator.paused = false;
    this.controls.pos.y = APP.h / 2 + 100;
    return this.controls.animate('slide', {
      change: new Vector(0, -150),
      f: Easie.elasticOut,
      duration: 1.5
    });
  };

  Scene2.prototype.stop = function() {
    var d, winner;
    this.indicator.paused = true;
    winner = this.indicator.getWinner();
    d = APP.w;
    this.loser = this.player2;
    if (winner === this.player2) {
      this.loser = this.player1;
      d *= -1;
    }
    this.loser.animate('slide', {
      change: new Vector(d, 0),
      duration: 0.5
    });
    this.loser.animate('rotate', {
      change: -4 * Math.PI
    });
    winner.animate('slide', {
      change: new Vector(-winner.pos.x, 0),
      duration: 0.5
    });
    this.indicator.animate('slide', {
      change: new Vector(-this.indicator.pos.x, 0),
      duration: 0.5
    });
    this.indicator.animate('pulse');
    return this.controls.animate('slide', {
      change: new Vector(0, 150),
      f: Easie.expoOut
    });
  };

  Scene2.prototype.update = function(delta, inputs) {
    var input, _i, _len;
    Scene2.__super__.update.call(this, delta, inputs);
    this.introText.color.update(delta);
    if (this.time === -1) {
      this.cooldown -= delta;
      if (this.cooldown <= 0) {
        if (this.playAgain.hidden) {
          this.playAgain.hidden = false;
          this.playAgain.scale.set(0, 0);
          this.playAgain.animate('pulse', {
            duration: 2
          });
        }
        if (inputs.length) {
          this.reset();
        }
      }
      return;
    }
    for (_i = 0, _len = inputs.length; _i < _len; _i++) {
      input = inputs[_i];
      if (input.command === 'tap') {
        this.tap(input.args.x, input.args.y);
      }
    }
    this.time += delta;
    if (this.time >= CFG.GAME_TIME) {
      this.time = -1;
      this.cooldown = 2;
      return this.stop();
    }
  };

  Scene2.prototype.tap = function(x, y) {
    var clicked1, clicked2, originalX, originalY;
    originalX = x;
    originalY = y;
    x = x - this.controls.pos.x;
    y = y - this.controls.pos.y;
    clicked1 = this.btn1.tap(x, y);
    clicked2 = this.btn2.tap(x, y);
    if (!(clicked1 || clicked2)) {
      return this.player1.pointArms(originalX, originalY);
    }
  };

  return Scene2;

})(Node);

var Arm, Head, Player, UpperBody, Wobbler,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Wobbler = (function() {
  function Wobbler(args) {
    this.time = 0;
    this.target = args.target;
    this.v = args.v || new Vector(0, 1);
    this.limit = args.limit || new Vector(0, 2);
    this.freq = args.freq || 10;
    this.start = this.target.pos.clone();
    this.tmp1 = new Vector;
    this.tmp2 = new Vector;
  }

  Wobbler.prototype.add = function(v) {
    this.v.add(v);
    if (this.v.x > this.limit.x) {
      this.v.x = this.limit.x;
    }
    if (this.v.y > this.limit.y) {
      return this.v.y = this.limit.y;
    }
  };

  Wobbler.prototype.update = function(delta) {
    this.time += delta * this.freq;
    this.v.scale(0.999);
    this.tmp1.set(this.v.x, this.v.y);
    this.tmp1.scale(Math.sin(this.time));
    this.tmp2.set(this.start.x, this.start.y);
    this.tmp2.add(this.tmp1);
    return this.target.pos.set(this.tmp2.x, this.tmp2.y);
  };

  return Wobbler;

})();

Head = (function(_super) {
  __extends(Head, _super);

  function Head(args) {
    Head.__super__.constructor.call(this, args);
    this.attach(new RoundedRect({
      color: args.skinColor,
      w: 50,
      h: 50,
      r: 5
    }));
    this.attach(new RoundedRect({
      color: CFG.STD_FG_COLOR,
      w: 10,
      h: 10,
      r: 5,
      pos: new Vector(-10, 0)
    }));
    this.attach(new RoundedRect({
      color: CFG.STD_FG_COLOR,
      w: 10,
      h: 10,
      r: 5,
      pos: new Vector(10, 0)
    }));
  }

  return Head;

})(Node);

Arm = (function(_super) {
  __extends(Arm, _super);

  function Arm(args) {
    var LEN;
    Arm.__super__.constructor.call(this, args);
    LEN = 70;
    this.attach(new RoundedRect({
      color: args.skinColor,
      w: LEN,
      h: 10,
      r: 5,
      pos: new Vector(LEN / 2, 0)
    }));
    this.rotation = Math.PI / 2;
  }

  Arm.prototype.pointTo = function(p) {
    var change, newRot;
    p.sub(this.pos);
    p.norm();
    newRot = Math.atan2(p.y, p.x);
    change = newRot - this.rotation;
    change += 2 * Math.PI;
    this.controllers = [];
    return this.animate('rotate', {
      change: change,
      duration: 0.1
    });
  };

  return Arm;

})(Node);

UpperBody = (function(_super) {
  __extends(UpperBody, _super);

  function UpperBody(args) {
    UpperBody.__super__.constructor.call(this, args);
    this.torso = new RoundedRect({
      color: CFG.STD_FG_COLOR,
      w: 50,
      h: 90,
      r: 5
    });
    this.head = new Head({
      skinColor: args.skinColor,
      pos: new Vector(0, -60)
    });
    this.wobbler = new Wobbler({
      target: this,
      v: new Vector(0, 0),
      limit: new Vector(0, 20),
      freq: 10
    });
    this.head.wobbler = new Wobbler({
      target: this.head,
      v: new Vector(0, 0),
      limit: new Vector(15, 0),
      freq: 10
    });
    this.attachController(this.wobbler);
    this.head.attachController(this.head.wobbler);
    this.arm1 = new Arm({
      skinColor: args.skinColor,
      pos: new Vector(-30, -35)
    });
    this.arm2 = new Arm({
      skinColor: args.skinColor,
      pos: new Vector(30, -35)
    });
    this.attach(this.torso);
    this.attach(this.arm1);
    this.attach(this.arm2);
    this.attach(this.head);
  }

  return UpperBody;

})(Node);

Player = (function(_super) {
  __extends(Player, _super);

  function Player(args) {
    Player.__super__.constructor.call(this, args);
    this.handicap = args.handicap || 1;
    this.body = new UpperBody({
      skinColor: args.skinColor
    });
    this.leg1 = new RoundedRect({
      color: args.skinColor,
      w: 10,
      h: 50,
      r: 5,
      pos: new Vector(-20, 60)
    });
    this.leg2 = new RoundedRect({
      color: args.skinColor,
      w: 10,
      h: 50,
      r: 5,
      pos: new Vector(20, 60)
    });
    this.attach(this.body);
    this.attach(this.leg1);
    this.attach(this.leg2);
    this.reset();
  }

  Player.prototype.reset = function() {
    this.rotation = 0;
    this.interactions = 0;
    return this.controllers = [];
  };

  Player.prototype.getScore = function() {
    return this.interactions * this.handicap;
  };

  Player.prototype.pointArms = function(x, y) {
    this.interactions++;
    x = x - this.pos.x;
    y = y - this.pos.y;
    if (x < 0) {
      this.body.arm1.pointTo(new Vector(x, y));
    }
    if (x > 0) {
      return this.body.arm2.pointTo(new Vector(x, y));
    }
  };

  Player.prototype.wobbleBody = function() {
    this.interactions++;
    return this.body.wobbler.add(new Vector(0, CFG.TAP_POWER));
  };

  Player.prototype.wobbleHead = function() {
    this.interactions++;
    return this.body.head.wobbler.add(new Vector(CFG.TAP_POWER, 0));
  };

  return Player;

})(Node);

var AIPlayer,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

AIPlayer = (function(_super) {
  __extends(AIPlayer, _super);

  function AIPlayer(args) {
    AIPlayer.__super__.constructor.call(this, args);
    this.time = -1;
  }

  AIPlayer.prototype.start = function() {
    var x, y;
    this.time = 0;
    x = Random.int(this.pos.x - 100, this.pos.x - 200);
    y = Random.int(this.pos.y + 100, this.pos.y - 100);
    this.pointArms(x, y);
    x = Random.int(this.pos.x + 100, this.pos.x + 200);
    y = Random.int(this.pos.y + 100, this.pos.y - 100);
    return this.pointArms(x, y);
  };

  AIPlayer.prototype.pulse = function() {
    this.wobbleHead();
    this.wobbleHead();
    this.wobbleHead();
    this.wobbleBody();
    this.wobbleBody();
    return this.wobbleBody();
  };

  AIPlayer.prototype.update = function(delta) {
    var x, y;
    AIPlayer.__super__.update.call(this, delta);
    if (this.time === -1) {
      return;
    }
    this.time -= delta;
    if (this.time <= 0) {
      x = Random.int(APP.w / -2, APP.w / 2);
      y = Random.int(APP.h / -2, APP.h / 2);
      this.pointArms(x, y);
      this.pulse();
      return this.time = Random(0.1, 0.5);
    }
  };

  return AIPlayer;

})(Player);

var House,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

House = (function(_super) {
  __extends(House, _super);

  function House(args) {
    var PADDING, STEP, WINDOW_H, WINDOW_W, cols, i, j, rows, _i, _j;
    House.__super__.constructor.call(this, args);
    this.attach(new RoundedRect({
      color: new Color(150, 150, 150),
      r: 5,
      h: args.h,
      w: args.h,
      pos: new Vector(0, -args.h / 2)
    }));
    WINDOW_W = 10;
    WINDOW_H = 10;
    PADDING = 10;
    STEP = WINDOW_W + PADDING;
    cols = (args.w - 2 * PADDING) / STEP;
    rows = (args.h - 2 * PADDING) / STEP;
    this.attach(new RoundedRect({
      color: new Color(150, 150, 150),
      r: 5,
      h: args.h,
      w: args.h,
      pos: new Vector(0, -args.h / 2)
    }));
    for (i = _i = 0; 0 <= rows ? _i < rows : _i > rows; i = 0 <= rows ? ++_i : --_i) {
      for (j = _j = 0; 0 <= cols ? _j < cols : _j > cols; j = 0 <= cols ? ++_j : --_j) {
        this.attach(new RoundedRect({
          color: new Color(50, 50, 50),
          r: 5,
          h: 5,
          w: 5,
          pos: new Vector(-args.w + j * STEP, -args.h / 2 + i * STEP)
        }));
      }
    }
  }

  return House;

})(Node);

var SongTitle,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

SongTitle = (function(_super) {
  __extends(SongTitle, _super);

  function SongTitle() {
    this.d = CFG.STD_FONT_SIZE + CFG.PADDING;
    SongTitle.__super__.constructor.call(this, {
      text: '',
      textAlign: 'center',
      pos: new Vector(0, (APP.h / -2) - this.d),
      hidden: true
    });
    this.stdPos = this.pos.clone();
    this.time = 0;
  }

  SongTitle.prototype.update = function(delta) {
    SongTitle.__super__.update.call(this, delta);
    if (this.time === -1) {
      return;
    }
    this.time += delta;
    if (this.time > 5) {
      return this.hide();
    }
  };

  SongTitle.prototype.show = function() {
    this.controllers = [];
    this.pos.y = this.stdPos.y;
    this.hidden = false;
    this.animate('slide', {
      change: new Vector(0, 2 * this.d),
      f: Easie.elasticOut,
      duration: 1
    });
    return this.time = 0;
  };

  SongTitle.prototype.hide = function() {
    this.controllers = [];
    this.pos.y = this.stdPos.y + this.d;
    this.animate('slide', {
      change: new Vector(0, -this.d),
      duration: 0.1
    });
    return this.time = -1;
  };

  return SongTitle;

})(Text);

var APP, blurred, canvas, frame;

canvas = document.getElementById('canvas');

APP = new Application(canvas);

APP.start();

blurred = false;

if (typeof ejecta === "undefined") {
  window.onfocus = (function(_this) {
    return function() {
      console.log('focus');
      blurred = false;
      APP.last = null;
      return frame();
    };
  })(this);
  window.onblur = (function(_this) {
    return function() {
      console.log('blur');
      return blurred = true;
    };
  })(this);
}

frame = function() {
  if (blurred) {
    return;
  }
  APP.update();
  APP.render();
  return requestAnimationFrame(frame);
};

frame();
