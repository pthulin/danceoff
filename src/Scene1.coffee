class Scene1 extends Node
  constructor: (args) ->
    super args

    skyColor = new Color 49, 163, 233

    @attach new Rect {
      color: skyColor.clone()
      w: APP.w
      h: APP.h
    }

    @attach new Text {
      text: "On the surface"
      pos: new Vector 0, -50
    }

    @t2 = new Text {
      text: "Everything looks calm"
      pos: new Vector 0, 0
      color: skyColor.clone()
    }

    @animate 'slide', {
      duration: 1.5
      onDone: =>
        @t2.color.fadeTo CFG.STD_FG_COLOR
    }

    @attach @t2

  update: (delta) ->
    super delta
    @t2.color.update delta
