class AIPlayer extends Player
  constructor: (args) ->
    super args
    @time = -1

  start: ->
    @time = 0

    # Left arm
    x = Random.int @pos.x - 100, @pos.x - 200
    y = Random.int @pos.y + 100, @pos.y - 100
    @pointArms x, y

    # Right arm
    x = Random.int @pos.x + 100, @pos.x + 200
    y = Random.int @pos.y + 100, @pos.y - 100
    @pointArms x, y

  pulse: ->
    @wobbleHead()
    @wobbleHead()
    @wobbleHead()

    @wobbleBody()
    @wobbleBody()
    @wobbleBody()

  update: (delta) ->
    super delta

    if @time == -1 then return

    @time -= delta

    if @time <= 0
      x = Random.int APP.w/-2, APP.w/2
      y = Random.int APP.h/-2, APP.h/2

      @pointArms x, y

      @pulse()

      @time = Random 0.1, 0.5
