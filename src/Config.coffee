CFG =
  DEBUG_ENABLED: true

  STD_BG_COLOR: new Color 120, 120, 120
  STD_FG_COLOR: new Color 0, 0, 0
  STD_LINE_WIDTH: 4
  STD_FONT: 'Lemon-Regular'
  STD_FONT_SIZE: 20
  STD_FONT_SIZE_BIG: 50

  GAME_TIME: 10

  TAP_POWER: 4

  SKIN_COLORS: [
    new Color 251, 71, 87
    new Color 136, 225, 80
  ]

  PADDING: 8

  SONG_TITLES: {
    'song1': 'Professor Kliq - Bust This Bust That'
  }

  SOUNDS: {
    'song1': 'laut.mp3'
    # 'song1': 'Professor_Kliq_-_03_-_Bust_This_Bust_That.mp3'
  }

if window.innerWidth >= 768
  CFG.STD_FONT_SIZE = 30
  CFG.STD_FONT_SIZE_BIG = 70

if typeof ejecta isnt "undefined"
  ejecta.loadFont "assets/fonts/" + CFG.STD_FONT + ".ttf"
