class SongTitle extends Text
  constructor: ->
    @d = CFG.STD_FONT_SIZE+CFG.PADDING

    super {
      text: ''
      textAlign: 'center'
      pos: new Vector 0, (APP.h/-2)-@d
      hidden: true
    }

    @stdPos = @pos.clone()
    @time = 0

  update: (delta) ->
    super delta

    if @time == -1 then return

    @time += delta

    if @time > 5 then @hide()

  show: ->
    @controllers = []
    @pos.y = @stdPos.y
    @hidden = false

    @animate 'slide', {
      change: new Vector 0, 2*@d
      f: Easie.elasticOut
      duration: 1
    }

    @time = 0

  hide: ->
    @controllers = []
    @pos.y = @stdPos.y+@d

    @animate 'slide', {
      change: new Vector 0, -@d
      duration: 0.1
    }

    @time = -1
