class House extends Node
  constructor: (args) ->
    super args

    @attach new RoundedRect {
      color: new Color 150, 150, 150
      r: 5
      h: args.h
      w: args.h
      pos: new Vector 0, -args.h/2
    }

    WINDOW_W = 10
    WINDOW_H = 10
    PADDING = 10
    STEP = WINDOW_W+PADDING

    cols = (args.w-2*PADDING) / STEP
    rows = (args.h-2*PADDING) / STEP

    @attach new RoundedRect {
      color: new Color 150, 150, 150
      r: 5
      h: args.h
      w: args.h
      pos: new Vector 0, -args.h/2
    }

    for i in [0...rows]
      for j in [0...cols]
        @attach new RoundedRect {
          color: new Color 50, 50, 50
          r: 5
          h: 5
          w: 5
          pos: new Vector -args.w+j*STEP, -args.h/2+i*STEP
        }


