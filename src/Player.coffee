class Wobbler
  constructor: (args) ->
    @time = 0

    @target = args.target

    @v = args.v or new Vector 0, 1
    @limit = args.limit or new Vector 0, 2

    @freq = args.freq or 10

    @start = @target.pos.clone()

    @tmp1 = new Vector
    @tmp2 = new Vector

  add: (v) ->
    @v.add v

    if @v.x > @limit.x then @v.x = @limit.x
    if @v.y > @limit.y then @v.y = @limit.y

  update: (delta) ->
    @time += delta * @freq

    @v.scale 0.999

    @tmp1.set @v.x, @v.y
    @tmp1.scale Math.sin @time 

    @tmp2.set @start.x, @start.y
    @tmp2.add @tmp1

    @target.pos.set @tmp2.x, @tmp2.y

class Head extends Node
  constructor: (args) ->
    super args

    @attach new RoundedRect {
      color: args.skinColor
      w: 50
      h: 50
      r: 5
    }

    @attach new RoundedRect {
      color: CFG.STD_FG_COLOR
      w: 10
      h: 10
      r: 5
      pos: new Vector -10, 0
    }

    @attach new RoundedRect {
      color: CFG.STD_FG_COLOR
      w: 10
      h: 10
      r: 5
      pos: new Vector 10, 0
    }

class Arm extends Node
  constructor: (args) ->
    super args

    LEN = 70

    @attach new RoundedRect {
      color: args.skinColor
      w: LEN
      h: 10
      r: 5
      pos: new Vector LEN/2, 0
    }

    @rotation = Math.PI/2

  pointTo: (p) ->
    p.sub @pos
    p.norm()

    newRot = Math.atan2 p.y, p.x 

    # if newRot < 0 and newRot > Math.PI/-2
    #   newRot = 2*Math.PI - newRot

    change = newRot - @rotation
    change += 2*Math.PI

    @controllers = []
    @animate 'rotate', {
      change: change
      duration: 0.1
    }

class UpperBody extends Node
  constructor: (args) ->
    super args

    @torso = new RoundedRect {
      color: CFG.STD_FG_COLOR
      w: 50
      h: 90
      r: 5
    }

    @head = new Head {
      skinColor: args.skinColor
      pos: new Vector 0, -60
    }

    @wobbler = new Wobbler {
      target: @
      v: new Vector 0, 0
      limit: new Vector 0, 20
      freq: 10
    }

    @head.wobbler = new Wobbler {
      target: @head
      v: new Vector 0, 0
      limit: new Vector 15, 0
      freq: 10
    }

    @attachController @wobbler
    @head.attachController @head.wobbler

    @arm1 = new Arm {
      skinColor: args.skinColor
      pos: new Vector -30, -35
    }

    @arm2 = new Arm {
      skinColor: args.skinColor
      pos: new Vector 30, -35
    }

    @attach @torso
    @attach @arm1
    @attach @arm2
    @attach @head

class Player extends Node
  constructor: (args) ->
    super args

    @handicap = args.handicap or 1

    @body = new UpperBody {
      skinColor: args.skinColor
    }

    @leg1 = new RoundedRect {
      color: args.skinColor
      w: 10
      h: 50
      r: 5
      pos: new Vector -20, 60
    }

    @leg2 = new RoundedRect {
      color: args.skinColor
      w: 10
      h: 50
      r: 5
      pos: new Vector 20, 60
    }

    @attach @body
    @attach @leg1
    @attach @leg2

    @reset()

  reset: ->
    @rotation = 0
    @interactions = 0
    @controllers = []

  getScore: ->
    return @interactions*@handicap

  pointArms: (x, y) ->
    @interactions++

    x = x - @pos.x
    y = y - @pos.y

    if x < 0 then @body.arm1.pointTo new Vector x, y
    if x > 0 then @body.arm2.pointTo new Vector x, y

  wobbleBody: ->
    @interactions++
    @body.wobbler.add new Vector 0, CFG.TAP_POWER 

  wobbleHead: ->
    @interactions++
    @body.head.wobbler.add new Vector CFG.TAP_POWER, 0
