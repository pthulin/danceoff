class RootNode extends Node
  constructor: ->
    super()

    @scene1 = new Scene1 {
      pos: new Vector
    }

    @scene2 = new Scene2 {
      pos: new Vector 0, APP.h
    }

    @attach @scene1
    @attach @scene2

  start: ->
    @animate 'slide', {
      duration: 3
      onDone: =>
        @animate 'slide', {
          change: new Vector 0, -APP.h
          f: Easie.cubicOut
          duration: 5
          onDone: =>
            @scene2.start()
        }
    }

