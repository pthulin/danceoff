class Indicator extends Node
  constructor: (args) ->
    super args

    W = 15
    H = 15

    @player1 = args.player1
    @player2 = args.player2

    @attach new Polygon {
      color: CFG.STD_FG_COLOR
      vertices: [
        new Vector W/-2, H/-2
        new Vector W/2,  H/-2
        new Vector 0,    H/2
      ]
    }

    @attach new Text {
      text: 'Winner'
      pos: new Vector 0, -25
      fontSize: 16
    }

    @paused = false

  getWinner: ->
    if @pos.x > 0 then return @player2
    return @player1

  update: (delta) ->
    super delta

    if @paused then return

    newX = @player2.getScore() - @player1.getScore()

    if newX < @player1.pos.x then newX = @player1.pos.x
    if newX > @player2.pos.x then newX = @player2.pos.x

    d = newX - @pos.x

    @pos.x += d*delta

class CircButton extends Node
  constructor: (args) ->
    super args

    @callback = args.callback

    @n = new Node

    @c1 = new Circle {
      color: new Color 200, 200, 200
      radius: 40
    }

    @c2 = new Circle {
      color: new Color 20, 20, 20
      radius: 40
      pos: new Vector 0, 5
    }

    @attach @c2
    @attach @n
    @n.attach @c1
    @n.attach args.icon

  down: ->
    @controllers = []
    @n.pos.y = 0
    @n.animate 'slide', {
      duration: 0.1
      change: new Vector 0, 5
      onDone: =>
        @up()
    }

  up: ->
    @controllers = []
    @n.pos.y = 5
    @n.animate 'slide', {
      duration: 0.1
      change: new Vector 0, -5
    }

  tap: (x, y) ->
    x = @pos.x-x
    y = @pos.y-y

    clicked = @c1.intersectsDot x, y

    if clicked
      @down()
      @callback()

    return clicked

class Scene2 extends Node
  constructor: (args) ->
    super args

    @controls = new Node {
      pos: new Vector 0, APP.h/2+150
    }

    @playAgain = new Text {
      text: 'Play again'
      pos: new Vector 0, APP.h/2-50
      hidden: true
    }

    @btn1 = new CircButton {
      pos: new Vector -50, 0
      icon: new Head {
        skinColor: CFG.SKIN_COLORS[0]
        scale: new Vector 0.5, 0.5
      }
      callback: =>
        @player1.wobbleHead()
    }

    @btn2 = new CircButton {
      pos: new Vector 50, 0
      icon: new Player {
        skinColor: CFG.SKIN_COLORS[0]
        scale: new Vector 0.3, 0.3
      }
      callback: =>
        @player1.wobbleBody()
    }

    @player1 = new Player {
      skinColor: CFG.SKIN_COLORS[0]
      pos: new Vector -100, 0
    }

    @player2 = new AIPlayer {
      skinColor: CFG.SKIN_COLORS[1]
      handicap: 0.1
      pos: new Vector 100, 0
    }

    @indicator = new Indicator {
      player1: @player1
      player2: @player2
      pos: new Vector 0, -150
      hidden: true
    }

    @attach @player1
    @attach @player2

    @controls.attach @btn1
    @controls.attach @btn2

    @attach @indicator
    @attach @controls
    @attach @playAgain

    @introText = new Text {
      text: "But under, there's a battle"
      pos: new Vector 0, -APP.h/2+CFG.STD_FONT_SIZE+CFG.PADDING
    }

    @attach @introText

  start: ->
    @introText.color.fadeTo CFG.STD_BG_COLOR

    # Dummy/pause
    @animate 'slide', {
      duration: 1.5
      onDone: =>
        @reset()
    }

  reset: ->
    Sound.play 'song1', true

    @playAgain.hidden = true

    @player1.reset()
    @player2.reset()

    @player1.pos = new Vector -100, 0
    @player2.pos = new Vector 100, 0

    @introText.hidden = true

    @player2.start()
    @time = 0

    @indicator.hidden = false
    @indicator.paused = false

    @controls.pos.y = APP.h/2+100
    @controls.animate 'slide', {
      change: new Vector 0, -150
      f: Easie.elasticOut
      duration: 1.5
    }

  stop: ->
    @indicator.paused = true

    winner = @indicator.getWinner()

    d = APP.w
    @loser = @player2

    if winner == @player2
      @loser = @player1
      d *= -1

    @loser.animate 'slide', {
      change: new Vector d, 0
      duration: 0.5
    }

    @loser.animate 'rotate', {
      change: -4*Math.PI
    }

    winner.animate 'slide', {
      change: new Vector -winner.pos.x, 0
      duration: 0.5
    }

    @indicator.animate 'slide', {
      change: new Vector -@indicator.pos.x, 0
      duration: 0.5
    }

    @indicator.animate 'pulse'

    @controls.animate 'slide', {
      change: new Vector 0, 150
      f: Easie.expoOut
    }

  update: (delta, inputs) ->
    super delta, inputs

    @introText.color.update delta

    if @time == -1
      @cooldown -= delta
      if @cooldown <= 0

        if @playAgain.hidden
          @playAgain.hidden = false
          @playAgain.scale.set 0, 0
          @playAgain.animate 'pulse', {
            duration: 2
          }

        if inputs.length then @reset()
      return

    for input in inputs
      if input.command == 'tap'
        @tap input.args.x, input.args.y

    @time += delta

    if @time >= CFG.GAME_TIME
      @time = -1
      @cooldown = 2
      @stop()

  tap: (x, y) ->
    originalX = x
    originalY = y

    x = x - @controls.pos.x
    y = y - @controls.pos.y

    clicked1 = @btn1.tap x, y
    clicked2 = @btn2.tap x, y

    unless clicked1 or clicked2
      @player1.pointArms originalX, originalY
