class Entity
  constructor: (args={}) ->
    @pos = args.pos or new Vector 0, 0
    @rotation = args.rotation or 0
    @scale = args.scale or new Vector 1, 1
    @hidden = args.hidden or false
    @controllers = args.controllers or []

  clear: ->
    @controllers = []

  attachController: (controller) ->
    @controllers.push controller

  detachController: (controller) ->
    @controllers = @controllers.filter (i) -> i isnt controller

  update: (delta, inputs) ->
    for controller in @controllers
      controller.update delta, inputs

  render: (canvas, ctx) ->
    ctx.translate @pos.x, @pos.y

    if @hFlipped
      ctx.rotate @rotation + Math.PI
      ctx.scale -@scale.x, @scale.y
    else
      ctx.rotate @rotation
      ctx.scale @scale.x, @scale.y

  animate: (type, args={}) ->
    if type == 'wiggle'
      args.type = 'rotate'
      args.f = args.f or Easie.elasticOut
      args.begin = args.begin or  -1
      args.change = args.change or 1
    else if type == 'rotate'
      args.type = 'rotate'
      args.f = args.f or Easie.linearNone
      args.begin = args.begin or @rotation
      args.change = args.change or Math.PI
    else if type == 'shrink'
      args.type = 'scale'
      args.f = args.f or Easie.linearNone
      args.begin = args.begin or  1
      args.change = args.change or -1
    else if type == 'pulse'
      args.type = 'scale'
      args.f = args.f or Easie.elasticOut
      args.begin = args.begin or new Vector 0, 0
      args.change = args.change or new Vector 1, 1
    else if type == 'slide'
      args.type = 'translate'
      args.f = args.f or Easie.linearNone
      args.begin = args.begin or @pos.clone()
      args.change = args.change or new Vector

    animation = new Animation @, args

    @attachController animation

    return animation
