class Img extends Entity
  @cache: {}

  constructor: (args={}) ->
    super args

    src = args.src
    @w = args.w
    @h = args.h

    cache = @constructor.cache

    if cache.hasOwnProperty src
      @img = cache[src]
    else
      @img = new Image()
      @img.src = src
      cache[src] = @img

  render: (canvas, ctx) ->
    super canvas, ctx

    if not @img.width and @img.height then return

    w = @w or @img.width
    h = @w or @img.height
    
    ctx.drawImage @img, w/-2, h/-2, w, h
