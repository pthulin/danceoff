class InputHandler
  constructor: () ->

    document.addEventListener 'touchstart', (ev) =>
      args =
        x: ev.touches[0].pageX - APP.w/2
        y: ev.touches[0].pageY - APP.h/2
      @exec 'tap', args

    document.addEventListener 'mousedown', (ev) =>
      args =
        x: ev.x - APP.w/2
        y: ev.y - APP.h/2
      @exec 'tap', args

    @inputs = []

  getRecordedInputs: () ->
    return @inputs

  reset: () ->
    # @inputs.length = 0
    @inputs = [] # Memory leak?
    # @inputs.splice 0, @inputs.length
    # console.log @inputs

  exec: (command, args) ->
    # console.log command, args
    @inputs.push {
      command: command
      args: args
    }
