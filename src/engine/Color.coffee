class Color
  constructor: (@r=0, @g=0, @b=0) ->
    @time = 0
    @duration = -1
    @callback = null

  set: (@r, @g, @b) ->

  isFading: () ->
    return @time < @duration

  setFrom: (c) ->
    @set c.r, c.g, c.b

  clone: ->
    new Color @r, @g, @b

  update: (delta) ->
    if not @isFading() then return

    @time += delta

    @r = Easie.linearNone @time, @begin.r, @change.r, @duration
    @g = Easie.linearNone @time, @begin.g, @change.g, @duration
    @b = Easie.linearNone @time, @begin.b, @change.b, @duration

    if @to.r > @begin.r and @r > @to.r or @to.r < @begin.r and @r < @to.r then @r = @to.r
    if @to.g > @begin.g and @g > @to.g or @to.g < @begin.g and @g < @to.g then @g = @to.g
    if @to.b > @begin.b and @b > @to.b or @to.b < @begin.b and @b < @to.b then @b = @to.b

    @r = Math.round @r
    @g = Math.round @g
    @b = Math.round @b

    if @time >= @duration && @callback then @callback()

  toString: () ->
    "rgb(#{@r}, #{@g}, #{@b})"

  fadeTo: (@to, @duration=1, @callback=null) ->
    @time = 0
    @begin = @clone()
    @change = new Color @to.r-@begin.r, @to.g-@begin.g, @to.b-@begin.b


