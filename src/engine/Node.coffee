class Node extends Entity
  constructor: (args={}) ->
    super args
    @children = []

  clear: ->
    super()
    @children = []

  attach: (child) ->
    if not child
      console.log "Can not attach '#{child}'"
      return
    @children.push child

  detach: (child) ->
    @children = @children.filter (i) -> i isnt child

  update: (delta, inputs) ->
    super delta, inputs
    for child in @children
      child.update delta, inputs

  render: (canvas, ctx) ->
    super canvas, ctx

    for child in @children
      if child.hidden then continue
      ctx.save()
      child.render canvas, ctx
      ctx.restore()
