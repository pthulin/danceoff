class Button extends Node
  constructor: (args) ->
    super()

    @img1 = new Img {
      src: args.src1
      hidden: false
    }

    @img2 = new Img {
      src: args.src2
      hidden: true
    }

    @attach @img1
    @attach @img2

  press: () ->
    @img1.hidden = true
    @img2.hidden = false
