DEBUG = (text) ->
  APP.debug.setText text

class DebugDisplay extends Node
  constructor: ->
    super {
      pos: new Vector -APP.w/2, APP.h/2
    }

    @STAY_TIME = 2

    FONT_SIZE = 20

    @text = new Text {
      text: 'DebugDisplay'
      textAlign: 'left'
      pos: new Vector 0, -FONT_SIZE/2
      color: new Color 0, 0, 0
      font: 'Monospace'
      fontSize: FONT_SIZE
    }

    @attach @text

    @time = @STAY_TIME
    @hidden = true

  setText: (text) ->
    @time = 0
    @hidden = false
    @text.text = text

  update: (delta, inputs) ->
    unless CFG.DEBUG_ENABLED then return
    if @hidden then return

    super delta, inputs

    @time += delta
    @hidden = @time > @STAY_TIME

  render: (canvas, ctx) ->
    unless CFG.DEBUG_ENABLED then return
    if @hidden then return
    super canvas, ctx
