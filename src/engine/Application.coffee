class Application
  constructor: (@canvas) ->
    console.log 'application started'

    @ctx = @canvas.getContext '2d'

    @w = window.innerWidth
    @h = window.innerHeight

    if typeof ejecta is "undefined"
      @retinaCanvasHack()
      window.onresize = =>
        @retinaCanvasHack()

    @ctx = @canvas.getContext '2d'

    for id, file of CFG.SOUNDS
      Sound.load id, file

  start: () ->
    @input = new InputHandler
    @background = new Background
    @debug = new DebugDisplay
    @root = new RootNode
    @root.start()

  retinaCanvasHack: () ->
    @scaleFactor = if window.devicePixelRatio then window.devicePixelRatio else 1
     
    console.log 'scaleFactor', @scaleFactor

    @canvas.width = @w * @scaleFactor
    @canvas.height = @h * @scaleFactor

    @canvas.style.width = @w + 'px'
    @canvas.style.height = @h + 'px'

    @ctx.scale @scaleFactor, @scaleFactor

  update: () ->
    now = new Date().getTime()
    delta = if @last then (now - @last) / 1000 else 0

    inputs = @input.getRecordedInputs()
    @input.reset()

    @background.update delta, inputs
    @root.update delta, inputs
    @debug.update delta, inputs

    @last = now
    
  render: () ->
    @ctx.save()
    @ctx.translate @w/2, @h/2

    @background.render @canvas, @ctx
    @root.render @canvas, @ctx
    @debug.render @canvas, @ctx

    @ctx.restore()
