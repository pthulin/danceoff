class Background
  constructor: () ->
    @color = CFG.STD_BG_COLOR.clone()

  update: (delta) ->
    @color.update delta

  render: (canvas, ctx) ->
    left = -APP.w/2
    top = -APP.h/2
    ctx.fillStyle = @color.toString()
    ctx.fillRect left, top, APP.w, APP.h
