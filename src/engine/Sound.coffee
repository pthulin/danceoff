class @Sound

  @cache = {}
  @PATH = 'assets/audio'

  @load: (id, file) ->
    src = @PATH + '/' + file

    audio = new Audio src
    audio.preload = true
    audio.load()

    @cache[id] = audio

  @play: (id, repeat) ->
    if not @cache.hasOwnProperty id
      console.log "Unknown sound '#{id}'"
      return

    audio = @cache[id]

    if repeat
      audio.loop = true

    audio.play()

