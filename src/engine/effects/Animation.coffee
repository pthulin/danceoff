class Animation
  constructor: (@target, args={}) ->
    @type = args.type or null

    @f = args.f or Easie.linearNone
    @begin = args.begin or null
    @change = args.change or null
    @duration = args.duration or 1

    @repeat = args.repeat or false
    @skipFirstRun = args.skipFirstRun or false

    @onDone = args.onDone or null
    @onWakeUp = args.onWakeUp or null
    
    @reset()

    if @skipFirstRun
      @dead = true
      @repeatIn = @repeat

  reset: () ->
    @current = 0
    @repeatIn = 0
    @paused = false
    @dead = false

  wakeUp: () ->
    @reset()
    if @onWakeUp
      @onWakeUp @

  pause: () ->
    @paused = true

  resume: () ->
    @paused = false

  update: (delta) ->
    if @paused then return

    if @dead
      if @repeatIn > 0
        @repeatIn -= delta
        if @repeatIn <= 0 then @wakeUp()
      return

    @current += delta

    if @current > @duration
      @dead = true
      if @onDone then @onDone @
      if @repeat then @repeatIn = @repeat
      return

    @step()

  step: () ->
    if @type == 'translate'
      @target.pos.x = @f @current, @begin.x, @change.x, @duration
      @target.pos.y = @f @current, @begin.y, @change.y, @duration
    else if @type == 'rotate'
      @target.rotation = @f @current, @begin, @change, @duration
    else if @type == 'scale'
      @target.scale.x = @f @current, @begin.x, @change.x, @duration
      @target.scale.y = @f @current, @begin.y, @change.y, @duration
    else
      console.log "Unknown animation type '#{@type}'"
