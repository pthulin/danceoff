class Polygon extends Shape
  constructor: (args={}) ->
    super args
    @vertices = args.vertices or []
    
  render: (canvas, ctx) ->
    super canvas, ctx

    ctx.fillStyle = @color.toString()
    ctx.beginPath()
    # ctx.moveTo 0, 0

    for vertex in @vertices
      ctx.lineTo vertex.x, vertex.y
      
    ctx.closePath()
    ctx.fill()
