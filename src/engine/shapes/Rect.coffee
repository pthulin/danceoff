class Rect extends Shape
  constructor: (args={}) ->
    super args
    @w = args.w
    @h = args.h
    
  intersectsDot: (x, y) ->
  	left = @pos.x - @w/2
  	top = @pos.y - @h/2
  	bottom = top + @h
  	right = left + @w
  	return x > left and x < right and y > top and y < bottom

  render: (canvas, ctx)  ->
    super canvas, ctx
    
    if @color
      ctx.fillStyle = @color.toString()
      ctx.fillRect @w/-2, @h/-2, @w, @h

    if @stroke
      ctx.strokeStyle = @stroke.toString()
      ctx.lineWidth = @lineWidth
      ctx.strokeRect @w/-2, @h/-2, @w, @h
