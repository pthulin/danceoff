class RoundedRect extends Shape

  constructor: (args={}) ->
    super args
    @w = args.w
    @h = args.h
    @r = args.r

  render: (canvas, ctx) ->
    super canvas, ctx
    
    w = @w
    h = @h
    x = w/-2
    y = h/-2
    r = @r

    ctx.beginPath()
    ctx.moveTo x+r, y

    ctx.arcTo x+w, y,   x+w, y+h, r
    ctx.arcTo x+w, y+h, x,   y+h, r
    ctx.arcTo x,   y+h, x,   y,   r
    ctx.arcTo x,   y,   x+w, y,   r

    ctx.closePath()

    if @color
      ctx.fillStyle = @color.toString()
      ctx.fill()

    if @stroke
      ctx.strokeStyle = @stroke.toString()
      ctx.lineWidth = @lineWidth
      ctx.stroke()
