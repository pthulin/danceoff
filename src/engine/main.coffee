canvas = document.getElementById 'canvas'

APP = new Application canvas
APP.start()

blurred = false

if typeof ejecta is "undefined"
  window.onfocus = =>
    console.log 'focus'
    blurred = false
    APP.last = null
    frame()

  window.onblur = =>
    console.log 'blur'
    blurred = true

frame = () ->
  if blurred
    return
    
  APP.update()
  APP.render()
  
  requestAnimationFrame frame
    
frame()