module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.initConfig({
    coffee: {
      compile: {
        options: {
          bare: true,
          // sourceMap: true,
        },
        files: {
          'App/index.js': [
            'src/engine/math/Random.coffee',
            'src/engine/math/Vector.coffee',

            'src/engine/Color.coffee',
            'src/engine/Sound.coffee',
            'src/engine/Entity.coffee',
            'src/engine/Node.coffee',

            'src/engine/shapes/Shape.coffee',
            'src/engine/shapes/Circle.coffee',
            'src/engine/shapes/ProgressArc.coffee',
            'src/engine/shapes/Rect.coffee',
            'src/engine/shapes/RoundedRect.coffee',
            'src/engine/shapes/Polygon.coffee',
            'src/engine/shapes/Line.coffee',
            'src/engine/shapes/Text.coffee',

            'src/engine/Img.coffee',
            'src/engine/ui/Button.coffee',

            'src/engine/effects/Easie.coffee',
            'src/engine/effects/Animation.coffee',
            
            'src/engine/InputHandler.coffee',
            'src/engine/DebugDisplay.coffee',
            'src/engine/Background.coffee',
            'src/engine/Application.coffee',

            'src/Config.coffee',
            'src/Game.coffee',
            'src/Scene1.coffee',
            'src/Scene2.coffee',
            'src/Player.coffee',
            'src/AIPlayer.coffee',
            'src/House.coffee',
            'src/SongTitle.coffee',

            'src/engine/Main.coffee',
          ],
        }
      }
    },
    watch: {
      coffee: {
        files: ['src/**/*.coffee'],
        tasks: ['coffee'],
      },
    },
  });
  grunt.registerTask('default', ['coffee']);
};