"Danceoff" is an game made for the Ludum Dare 29 Compo.

The theme was "Beneith the Surface".

The game was made in 48 hours.

Tech used:

* my-little-game-engine: https://bitbucket.org/pthulin/my-little-game-engine
* GarageBand
* The "Lemon" Google Web Font: https://www.google.com/fonts/specimen/Lemon
